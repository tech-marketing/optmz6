# write a script to add log event into txt file every time user login-in
def log_user_login(username)
  timestamp = Time.now.strftime("%Y-%m-%d %H:%M:%S")
  log_entry = "#{timestamp} - User '#{username}' logged in\n"
  
  File.open('user_login.txt', 'a') do |file|
    file.write(log_entry)
  end
rescue StandardError => e
  puts "Error writing to log file: #{e.message}"


# sample usage
log_user_login("john_doe")
log_user_login("emily_smith")
    