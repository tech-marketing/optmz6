# add a script to add log into text file every time user login
def self.log_user_login(user)
  File.open('user_logins.log', 'a') do |file|
    timestamp = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    file.puts "#{timestamp} - User #{user.id} (#{user.email}) logged in"
  end
end

# add some sample data
def self.add_sample_data
  users = [
    { id: 1, email: 'john@example.com' },
    { id: 2, email: 'jane@example.com' },
    { id: 3, email: 'bob@example.com' }
  ]

  users.each do |user_data|
    user = OpenStruct.new(user_data)
    log_user_login(user)
  end
end